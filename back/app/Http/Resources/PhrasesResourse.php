<?php

namespace App\Http\Resources;

use App\Models\PhraseLikes;
use Illuminate\Http\Resources\Json\JsonResource;

class PhrasesResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $likes = PhraseLikes::where('phrase_id', $this->id)->count();
        $liked = PhraseLikes::where('user_id', $request->user()->id)->where('phrase_id', $this->id)->count();

        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'likes_count' => $likes,
            'liked' => $liked,
            'owner' => $this->user_id == $request->user()->id,
            'created_at' => $this->created_at->format('m/d/Y'),
            'updated_at' => $this->updated_at->format('m/d/Y'),
        ];
    }
}
