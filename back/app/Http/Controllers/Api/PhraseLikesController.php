<?php

namespace App\Http\Controllers\Api;

use App\Models\PhraseLikes;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController;
use App\Models\Phrases;

class PhraseLikesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $phrase = Phrases::find($id);

        if ( !$phrase ) {
            return $this->sendResponse('error', false, 'Такой фразы нет');
        }

        $previewLike = PhraseLikes::where('user_id', $request->user()->id)->where('phrase_id', $id);

        if ( $previewLike->count() ) {
            $previewLike->delete();
            return $this->sendResponse('deleted', true);
        }

        $like = new PhraseLikes();
        $like->user_id = $request->user()->id;
        $like->phrase_id = $id;

        $like->save();

        return $this->sendResponse('created', true);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PhraseLikes  $phraseLikes
     * @return \Illuminate\Http\Response
     */
    public function show(PhraseLikes $phraseLikes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PhraseLikes  $phraseLikes
     * @return \Illuminate\Http\Response
     */
    public function edit(PhraseLikes $phraseLikes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PhraseLikes  $phraseLikes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhraseLikes $phraseLikes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PhraseLikes  $phraseLikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhraseLikes $phraseLikes)
    {
        // return $this->sendResponse((ph))
    }
}
