<?php

namespace App\Http\Controllers\Api;

use Session;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UserController extends BaseController
{
    /**
     * Register
     */
    public function register(Request $request)
    {

        $rules = [
            'email' => 'required|unique:users|email',
            'password' => ['required', 'confirmed', Password::min(6)],
        ];

        $validate = Validator::make($request->all(), $rules);

        $success = false;
        $message = '';
        $errors = false;

        if ( $validate->fails() ) {

            $errors = $validate->errors();

        } else {

            try {
                $user = new User();
                $user->name = $request->input('email');
                $user->email = $request->input('email');
                $user->password = Hash::make($request->input('password'));
                $user->save();

                $success = true;
                $message = 'User register successfully';
            } catch (\Illuminate\Database\QueryException $e) {
                $success = false;
                $message = $e->getMessage();
            }
        }

        return $this->sendResponse($message, $success, $errors);
    }

    /**
     * Login
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $errors = false;
        if (Auth::attempt($credentials)) {
            $success = true;

            $authUser = Auth::user();
            $data = [];
            $data['id'] = $authUser->id;
            $data['token'] =  $authUser->createToken('MyAuthApp')->plainTextToken;
            $data['email'] = $authUser->email;

            $message = $data;
        } else {
            $success = false;
            $message = 'Unauthorised';
            $errors = 'Ошибка авторизации';
        }

        return $this->sendResponse($message, $success, $errors);
    }

    /**
     * Logout
     */
    public function logout(Request $request)
    {
        // try {
        //     Session::flush();
        //     $success = true;
        //     $message = 'Successfully logged out';
        // } catch (\Illuminate\Database\QueryException $ex) {
        //     $success = false;
        //     $message = $ex->getMessage();
        // }



        // return $this->sendResponse($message, $success);
        return $this->sendResponse($request->user(), true);
    }
}
