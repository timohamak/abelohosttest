<?php

namespace App\Http\Controllers\Api;

use App\Models\Phrases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\PhrasesResourse as Resource;
use App\Http\Controllers\Api\BaseController;

class PhrasesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_phrase = Phrases::all();
        $one_phrase = $all_phrase->count() ? $all_phrase->random(1) : [];
        return $this->sendResponse(Resource::collection($one_phrase), true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'text' => 'required'
        ]);

        if($validation->fails()){
            return $this->sendResponse('Error', false, $validation->errors());
        }

        $Phrase = new Phrases();
        $Phrase->title = $request->input('title');
        $Phrase->text = $request->input('text');
        $Phrase->user_id = $request->user()->id;
        $Phrase->save();

        return $this->sendResponse(new Resource($Phrase), true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Phrases  $phrases
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $phrase = Phrases::find($id);
        return $this->sendResponse(new Resource($phrase), true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Phrases  $phrases
     * @return \Illuminate\Http\Response
     */
    public function edit(Phrases $phrases)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Phrases  $phrases
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phrases $phrases)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Phrases  $phrases
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Phrases::where('id', $id)->delete();
        return $this->sendResponse('delete', (bool) $result);
    }
}
