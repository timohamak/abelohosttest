// import './plugins/axios'
import { createApp } from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import router from './router'
import store from './store'
import axios from 'axios'

const axiosInstance = axios.create({
  withCredentials: true,
  baseURL: 'http://localhost/'
})


loadFonts()

const app = createApp(App)

app.config.globalProperties.$axios = { ...axiosInstance }

app
.use(store)
.use(router)
.use(vuetify)
.mount('#app')
