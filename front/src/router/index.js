import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/phrases',
    name: 'phrases',
    component: () => import('../views/PhrasesView.vue'),
    meta: {
      auth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/LoginView.vue'),
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/RegisterView.vue'),
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if ( to.matched.some(e => e.meta.auth) ) {

    if( !localStorage.getItem('authUser') ) {

      router.push("/login");

    } else {

      next();

    }
  }

  next();
})

export default router
